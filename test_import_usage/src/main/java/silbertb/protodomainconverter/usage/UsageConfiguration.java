package silbertb.protodomainconverter.usage;

import org.silbertb.proto.domainconverter.annotations.ProtoConfigure;
import org.silbertb.proto.domainconverter.annotations.ProtoImport;
import silbertb.protodomainconverter.dep2.Dep2Configuration;
import silbertb.protodomainconverter.dep2.Dep2Converter;

@ProtoImport(Dep2Converter.class)
@ProtoConfigure(converterName = "silbertb.protodomainconverter.usage.UsageConverter")
public class UsageConfiguration {
}
