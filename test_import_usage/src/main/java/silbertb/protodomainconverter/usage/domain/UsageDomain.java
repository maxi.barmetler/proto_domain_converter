package silbertb.protodomainconverter.usage.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.model.proto.UsageProto;
import silbertb.protodomainconverter.dep1.domain.Dep1Domain;
import silbertb.protodomainconverter.dep2.domain.Dep2BuilderDomain;
import silbertb.protodomainconverter.dep2.domain.Dep2ConstructorBuilderDomain;
import silbertb.protodomainconverter.dep2.domain.Dep2ConstructorDomain;
import silbertb.protodomainconverter.dep2.domain.Dep2PojoDomain;

@Value
@Builder
@ProtoBuilder
@ProtoClass(protoClass = UsageProto.class, blacklist = true)
public class UsageDomain {
    double doubleValue;
    Dep2PojoDomain dep2Pojo;
    Dep2ConstructorDomain dep2Constructor;
    Dep2BuilderDomain dep2Builder;
    Dep2ConstructorBuilderDomain dep2BuilderConstructor;
    Dep1Domain dep1;
}
