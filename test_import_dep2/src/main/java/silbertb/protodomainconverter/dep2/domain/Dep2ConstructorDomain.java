package silbertb.protodomainconverter.dep2.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.model.proto.Dep2ConstructorProto;
import silbertb.protodomainconverter.dep1.domain.Dep1Domain;

@Data
@ProtoClass(protoClass = Dep2ConstructorProto.class)
public class Dep2ConstructorDomain {
    private final Dep1Domain dep1;

    @ProtoConstructor
    public Dep2ConstructorDomain(Dep1Domain dep1) {
        this.dep1 = dep1;
    }
}
