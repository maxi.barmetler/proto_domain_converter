package silbertb.protodomainconverter.dep2.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.model.proto.Dep2BuilderProto;
import silbertb.protodomainconverter.dep1.domain.Dep1Domain;

@Builder
@Value
@ProtoBuilder
@ProtoClass(protoClass = Dep2BuilderProto.class, blacklist = true)
public class Dep2BuilderDomain {
    Dep1Domain dep1;
}
