package silbertb.protodomainconverter.dep2;

import org.silbertb.proto.domainconverter.annotations.ProtoConfigure;
import org.silbertb.proto.domainconverter.annotations.ProtoImport;
import silbertb.protodomainconverter.dep1.Dep1Converter;

@ProtoImport(Dep1Converter.class)
@ProtoConfigure(converterName = "silbertb.protodomainconverter.dep2.Dep2Converter")
public class Dep2Configuration {
}
