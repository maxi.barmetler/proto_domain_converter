package org.silbertb.proto.domainconverter.collection;

import org.silbertb.proto.domainconverter.basic.BasicTestObjectsCreator;
import org.silbertb.proto.domainconverter.collection.domain.*;
import org.silbertb.proto.domainconverter.collection.domain.ConcreteMapToMessage;
import org.silbertb.proto.domainconverter.collection.domain.ConcretePrimitiveMap;
import org.silbertb.proto.domainconverter.collection.domain.MapToMessage;
import org.silbertb.proto.domainconverter.collection.domain.StringListDomain;
import org.silbertb.proto.domainconverter.test.proto.collection.*;

import java.util.*;

public class CollectionTestObjecsCreator {
    public static PrimitiveMap createPrimitiveMapDomain() {
        PrimitiveMap domain = new PrimitiveMap();
        domain.setPrimitiveMap(Map.of(1, 2L, 3, 4L));
        return domain;
    }

    public static PrimitiveMapProto createPrimitiveMapProto() {
        return PrimitiveMapProto.newBuilder()
                .putPrimitiveMap(1, 2)
                .putPrimitiveMap(3, 4)
                .build();
    }


    public static ConcreteMapToMessage createConcreteMapToMessageDomain() {
        ConcreteMapToMessage domain = new ConcreteMapToMessage();
        domain.setMapToMessage(new HashMap<>(Map.of(
                "aa", BasicTestObjectsCreator.createPrimitiveDomain(),
                "bb", BasicTestObjectsCreator.createPrimitiveDomain())));

        return domain;
    }

    public static ConcreteMapToMessageProto createConcreteMapToMessageProto() {
        return ConcreteMapToMessageProto.newBuilder()
                .putMapToMessage("aa", BasicTestObjectsCreator.createPrimitivesProto())
                .putMapToMessage("bb", BasicTestObjectsCreator.createPrimitivesProto())
                .build();
    }

    public static ConcretePrimitiveMap createConcretePrimitiveMapDomain() {
        ConcretePrimitiveMap domain = new ConcretePrimitiveMap();
        domain.setPrimitiveMap(new TreeMap<>(Map.of(1, 2L, 3, 4L)));
        return domain;
    }

    public static ConcretePrimitiveMapProto createConcretePrimitiveMapProto() {
        return ConcretePrimitiveMapProto.newBuilder()
                .putPrimitiveMap(1, 2)
                .putPrimitiveMap(3, 4)
                .build();
    }

    public static MapToMessage createMapToMessageDomain() {
        MapToMessage domain = new MapToMessage();
        domain.setMapToMessage(Map.of(
                "aa", BasicTestObjectsCreator.createPrimitiveDomain(),
                "bb", BasicTestObjectsCreator.createPrimitiveDomain()));
        return domain;
    }

    public static MapToMessageProto createMapToMessageProto() {
        return MapToMessageProto.newBuilder()
                .putMapToMessage("aa", BasicTestObjectsCreator.createPrimitivesProto())
                .putMapToMessage("bb", BasicTestObjectsCreator.createPrimitivesProto())
                .build();
    }

    public static PrimitiveList createPrimitiveListDomain() {
        PrimitiveList listDomain = new PrimitiveList();
        listDomain.setIntList(new ArrayList<>(List.of(1, 2, 3)));
        return listDomain;
    }

    public static PrimitiveListProto createPrimitiveListProto() {
        return PrimitiveListProto.newBuilder().addAllIntList(List.of(1, 2, 3)).build();
    }

    public static ConcretePrimitiveList createConcretePrimitiveListDomain() {
        ConcretePrimitiveList listDomain = new ConcretePrimitiveList();
        listDomain.setIntList(new LinkedList<>(List.of(1, 2, 3)));
        return listDomain;
    }

    public static ConcretePrimitiveListProto createConcretePrimitiveListProto() {
        return ConcretePrimitiveListProto.newBuilder().addAllIntList(List.of(1, 2, 3)).build();
    }

    public static StringListDomain createStringListDomain() {
        StringListDomain listDomain = new StringListDomain();
        listDomain.setStringList(new ArrayList<>(List.of("aa", "bb", "cc")));
        return listDomain;
    }

    public static StringListProto createStringListProto() {
        return StringListProto.newBuilder().addAllStringList(List.of("aa", "bb", "cc")).build();
    }

    public static MessageListDomain createMessageListDomain() {
        MessageListDomain listDomain = new MessageListDomain();
        listDomain.setMessageList(new ArrayList<>(
                List.of(BasicTestObjectsCreator.createPrimitiveDomain(), BasicTestObjectsCreator.createPrimitiveDomain())));
        return listDomain;
    }

    public static MessageListProto createMessageListProto() {
        return MessageListProto.newBuilder()
                .addMessageList(BasicTestObjectsCreator.createPrimitivesProto())
                .addMessageList(BasicTestObjectsCreator.createPrimitivesProto())
                .build();
    }

    public static ConcreteMessageList createConcreteMessageListDomain() {
        ConcreteMessageList listDomain = new ConcreteMessageList();
        listDomain.setMessageList(new LinkedList<>(
                List.of(BasicTestObjectsCreator.createPrimitiveDomain(), BasicTestObjectsCreator.createPrimitiveDomain())));
        return listDomain;
    }

    public static ConcreteMessageListProto createConcreteMessageListProto() {
        return ConcreteMessageListProto.newBuilder()
                .addMessageList(BasicTestObjectsCreator.createPrimitivesProto())
                .addMessageList(BasicTestObjectsCreator.createPrimitivesProto())
                .build();
    }


}
