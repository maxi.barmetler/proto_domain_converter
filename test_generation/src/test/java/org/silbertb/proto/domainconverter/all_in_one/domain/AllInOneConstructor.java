package org.silbertb.proto.domainconverter.all_in_one.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.collection.domain.ConcreteMapToMessage;
import org.silbertb.proto.domainconverter.collection.domain.MessageListDomain;
import org.silbertb.proto.domainconverter.basic.domain.BytesDomain;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.basic.domain.StringDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofBaseFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.test.proto.all_in_one.AllInOneConstructorProto;

@ToString
@EqualsAndHashCode
@ProtoClass(protoClass = AllInOneConstructorProto.class)
public class AllInOneConstructor {

    @ProtoConstructor
    public AllInOneConstructor(
            StringDomain strVal,
            @ProtoField BytesDomain bytesVal,
            @ProtoField ConcreteMapToMessage mapVal,
            @ProtoField MessageListDomain listVal,
            @OneofBase(oneOfFields = {
                    @OneofField(protoField = "oneof1_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
                    @OneofField(protoField = "oneof1_primitives", domainClass = PrimitiveDomain.class)
            })
            OneofBaseFieldDomain value1,
            @OneofBase(oneOfFields = {
                    @OneofField(protoField = "oneof2_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
                    @OneofField(protoField = "oneof2_primitives", domainClass = PrimitiveDomain.class)
            })
            OneofBaseFieldDomain value2) {
        this.strVal = strVal;
        this.bytesVal = bytesVal;
        this.mapVal = mapVal;
        this.listVal = listVal;
        this.value1 = value1;
        this.value2 = value2;
    }

    @Getter final private StringDomain strVal;
    @Getter final private BytesDomain bytesVal;
    @Getter final private ConcreteMapToMessage mapVal;
    @Getter final private MessageListDomain listVal;
    @Getter final private OneofBaseFieldDomain value1;
    @Getter final private OneofBaseFieldDomain value2;
}
