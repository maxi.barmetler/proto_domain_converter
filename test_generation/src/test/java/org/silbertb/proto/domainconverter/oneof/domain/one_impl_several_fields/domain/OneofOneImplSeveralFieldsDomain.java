package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.oneof.OneofOneImplSeveralFieldsProto;

@Data
@ProtoClass(protoClass = OneofOneImplSeveralFieldsProto.class)
public class OneofOneImplSeveralFieldsDomain {

    @OneofBase(oneofName = "field_converter", oneOfFields = {
            @OneofField(
                    protoField = "int_val",
                    domainClass = StringWrapperConstructor.class,
                    domainField = "strVal",
                    converter = IntStrConverter.class)
    })
    private OneofFieldsBase val1;

    @OneofBase(oneofName = "class_converter", oneOfFields = {
            @OneofField(
                    protoField = "double_val",
                    domainClass = StringWrapperConstructor.class,
                    converter = DoubleStringWrapperConstructorConverter.class)
    })
    private OneofFieldsBase val2;

    @OneofBase(oneofName = "no_converter", oneOfFields = {
            @OneofField(
                    protoField = "str_val",
                    domainClass = StringWrapperConstructor.class)
    })
    private OneofFieldsBase val3;
}
