package org.silbertb.proto.domainconverter.global_mapper;

import com.google.protobuf.ByteString;
import com.google.protobuf.Timestamp;
import org.silbertb.proto.domainconverter.global_mapper.domain.GlobalMappersContainerDomain;
import org.silbertb.proto.domainconverter.test.proto.GlobalMappersContainerProto;
import org.silbertb.proto.domainconverter.test.proto.IpAddressProto;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.time.Instant;

public class GlobalMapperTestObjectsCreator {

    public static GlobalMappersContainerDomain createGlobalMappersContainerDomain() throws UnknownHostException {
        return GlobalMappersContainerDomain.builder()
                .timestamp(Instant.ofEpochSecond(10000, 40))
                .ip(createInet4Address())
                .build();
    }

    public static GlobalMappersContainerProto createGlobalMappersContainerProto() {
        return GlobalMappersContainerProto.newBuilder()
                .setTimestamp(Timestamp.newBuilder().setSeconds(10000).setNanos(40).build())
                .setIp(createIpAddress())
                .build();
    }
    public static IpAddressProto createIpAddress() {
        return IpAddressProto.newBuilder()
                .setAddress(
                        ByteString.copyFrom(new byte[]{(byte) 0x7f, (byte) 0, (byte) 0, (byte) 0x1}))
                .build();
    }

    public static Inet4Address createInet4Address() throws UnknownHostException {
        return (Inet4Address)Inet4Address.getByName("127.0.0.1");
    }
}
