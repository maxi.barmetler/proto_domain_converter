package org.silbertb.proto.domainconverter.global_mapper;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.global_mapper.domain.GlobalMappersContainerDomain;
import org.silbertb.proto.domainconverter.test.proto.GlobalMappersContainerProto;

import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GlobalMapperTest {

    @Test
    public void testGlobalMappersToDomain() throws UnknownHostException {
        GlobalMappersContainerProto proto = GlobalMapperTestObjectsCreator.createGlobalMappersContainerProto();
        GlobalMappersContainerDomain domain = ProtoDomainConverter.toDomain(proto);
        GlobalMappersContainerDomain expected = GlobalMapperTestObjectsCreator.createGlobalMappersContainerDomain();

        assertEquals(expected, domain);
    }

    @Test
    public void testGlobalMappersToProto() throws UnknownHostException {
        GlobalMappersContainerDomain domain = GlobalMapperTestObjectsCreator.createGlobalMappersContainerDomain();
        GlobalMappersContainerProto proto = ProtoDomainConverter.toProto(domain);
        GlobalMappersContainerProto expected = GlobalMapperTestObjectsCreator.createGlobalMappersContainerProto();

        assertEquals(expected, proto);
    }
}
