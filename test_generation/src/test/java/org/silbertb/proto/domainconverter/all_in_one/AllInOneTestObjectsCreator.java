package org.silbertb.proto.domainconverter.all_in_one;

import org.silbertb.proto.domainconverter.basic.BasicTestObjectsCreator;
import org.silbertb.proto.domainconverter.all_in_one.domain.AllInOne;
import org.silbertb.proto.domainconverter.all_in_one.domain.AllInOneConstructor;
import org.silbertb.proto.domainconverter.collection.CollectionTestObjecsCreator;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.test.proto.all_in_one.AllInOneConstructorProto;
import org.silbertb.proto.domainconverter.test.proto.all_in_one.AllInOneProto;

public class AllInOneTestObjectsCreator {

    public static AllInOneProto createAllInOneProto() {
        return AllInOneProto.newBuilder()
                .setBytesVal(BasicTestObjectsCreator.createBytesProto())
                .setListVal(CollectionTestObjecsCreator.createMessageListProto())
                .setMapVal(CollectionTestObjecsCreator.createConcreteMapToMessageProto())
                .setOneof1IntVal(3)
                .setOneof2Primitives(BasicTestObjectsCreator.createPrimitivesProto())
                .build();
    }

    public static AllInOne createAllInOneDomain() {
        AllInOne domain = new AllInOne();
        domain.setBytesVal(BasicTestObjectsCreator.createBytesDomain());
        domain.setListVal(CollectionTestObjecsCreator.createMessageListDomain());
        domain.setMapVal(CollectionTestObjecsCreator.createConcreteMapToMessageDomain());
        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);
        domain.setValue1(oneofIntImplDomain);
        domain.setValue2(BasicTestObjectsCreator.createPrimitiveDomain());

        return domain;
    }

    public static AllInOneConstructorProto createAllInOneConstructorProto() {
        return AllInOneConstructorProto.newBuilder()
                .setStrVal(BasicTestObjectsCreator.createStringProto())
                .setBytesVal(BasicTestObjectsCreator.createBytesProto())
                .setListVal(CollectionTestObjecsCreator.createMessageListProto())
                .setMapVal(CollectionTestObjecsCreator.createConcreteMapToMessageProto())
                .setOneof1IntVal(3)
                .setOneof2Primitives(BasicTestObjectsCreator.createPrimitivesProto())
                .build();
    }

    public static AllInOneConstructor createAllInOneConstructorDomain() {
        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);
        return new AllInOneConstructor(
                BasicTestObjectsCreator.createStringDomain(),
                BasicTestObjectsCreator.createBytesDomain(),
                CollectionTestObjecsCreator.createConcreteMapToMessageDomain(),
                CollectionTestObjecsCreator.createMessageListDomain(),
                oneofIntImplDomain,
                BasicTestObjectsCreator.createPrimitiveDomain()
        );
    }
}
