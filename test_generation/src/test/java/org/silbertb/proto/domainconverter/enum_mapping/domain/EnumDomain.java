package org.silbertb.proto.domainconverter.enum_mapping.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.annotations.ProtoEnumValue;
import org.silbertb.proto.domainconverter.test.proto.enummapping.EnumProto;

@ProtoEnum(protoEnum = EnumProto.class)
public enum EnumDomain {
    @ProtoEnumValue(protoEnum = "FIRST")
    MY_FIRST,
    SECOND
}
