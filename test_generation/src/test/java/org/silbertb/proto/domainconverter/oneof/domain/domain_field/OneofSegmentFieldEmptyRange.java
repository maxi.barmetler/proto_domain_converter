package org.silbertb.proto.domainconverter.oneof.domain.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.oneof.EmptyRangeProto;

@ProtoClass(protoClass = EmptyRangeProto.class)
@Data
public class OneofSegmentFieldEmptyRange implements OneofSegmentField{
}
