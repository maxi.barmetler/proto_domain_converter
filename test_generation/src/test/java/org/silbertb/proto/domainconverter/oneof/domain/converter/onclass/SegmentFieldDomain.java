package org.silbertb.proto.domainconverter.oneof.domain.converter.onclass;


import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;

@Data
public class SegmentFieldDomain {
    @OneofBase(oneOfFields = {
            @OneofField(protoField = "point", domainClass = Point.class),
            @OneofField(protoField = "range", domainClass = Range.class)
    })
    private SegmentDomain segment;

}
