package org.silbertb.proto.domainconverter.custom_converter;

import org.silbertb.proto.domainconverter.custom_converter.domain.CustomConverter;
import org.silbertb.proto.domainconverter.custom_converter.domain.CustomListConverter;
import org.silbertb.proto.domainconverter.custom_converter.domain.CustomMapConverter;
import org.silbertb.proto.domainconverter.custom_converter.domain.MultiCustomConvertersDomain;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomConverterProto;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomListConverterProto;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomMapConverterProto;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.MultiCustomConvertersProto;

import java.util.HashMap;

public class CustomConverterTestObjectsCreator {

    public static CustomConverter createCustomConverterDomain() {
        CustomConverter domain = new CustomConverter();
        domain.setStrVal("5");
        return domain;
    }

    public static CustomConverterProto createCustomConverterProto() {
        return CustomConverterProto.newBuilder().setIntVal(5).build();
    }

    public static CustomListConverter createCustomListConverterDomain() {
        CustomListConverter domain = new CustomListConverter();
        domain.setCommaSeparatedInt("5,6");
        return domain;
    }

    public static CustomListConverterProto createCustomListConverterProto() {
        return CustomListConverterProto.newBuilder().addIntList(5).addIntList(6).build();
    }

    public static CustomMapConverter createCustomMapConverterDomain() {
        CustomMapConverter domain = new CustomMapConverter();
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");

        domain.setMap(map);
        return domain;
    }

    public static CustomMapConverterProto createCustomMapConverterProto() {
        return CustomMapConverterProto.newBuilder().putIntMap(1, 2).putIntMap(3, 4).build();
    }

    public static MultiCustomConvertersDomain createMultiCustomConvertersDomain() {
        MultiCustomConvertersDomain domain = new MultiCustomConvertersDomain();
        domain.setStrVal("5");
        domain.setCommaSeparatedInt("5,6");
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");

        domain.setMap(map);

        return domain;
    }

    public static MultiCustomConvertersProto createMultiCustomConvertersProto() {
        return MultiCustomConvertersProto.newBuilder()
                .setIntVal(5)
                .addIntList(5).addIntList(6)
                .putIntMap(1, 2).putIntMap(3, 4)
                .build();
    }

}
