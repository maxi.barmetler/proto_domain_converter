package org.silbertb.proto.domainconverter.enum_mapping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.enum_mapping.domain.EnumDomain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.enummapping.EnumProto;

public class EnumMappingTest {

    @Test
    public void testDefaultEnumMappingToDomain() {
        EnumDomain actual = ProtoDomainConverter.toDomain(EnumProto.SECOND);
        Assertions.assertEquals(EnumDomain.SECOND, actual);
    }

    @Test
    public void testDefaultEnumMappingToProto() {
        EnumProto actual = ProtoDomainConverter.toProto(EnumDomain.SECOND);
        Assertions.assertEquals(EnumProto.SECOND, actual);
    }

    @Test
    public void testEnumMappingDifferentNameToDomain() {
        EnumDomain actual = ProtoDomainConverter.toDomain(EnumProto.FIRST);
        Assertions.assertEquals(EnumDomain.MY_FIRST, actual);
    }

    @Test
    public void testEnumMappingDifferentNameToProto() {
        EnumProto actual = ProtoDomainConverter.toProto(EnumDomain.MY_FIRST);
        Assertions.assertEquals(EnumProto.FIRST, actual);
    }
}
