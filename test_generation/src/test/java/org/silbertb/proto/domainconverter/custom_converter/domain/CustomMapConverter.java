package org.silbertb.proto.domainconverter.custom_converter.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomMapConverterProto;

import java.util.HashMap;

@Data
@ProtoClass(protoClass = CustomMapConverterProto.class)
public class CustomMapConverter {

    @ProtoField(protoName = "int_map")
    @ProtoConverter(converter = IntMapStringMapConverter.class)
    private HashMap<String, String> map;
}
