package org.silbertb.proto.domainconverter.all_in_one;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.all_in_one.domain.AllInOne;
import org.silbertb.proto.domainconverter.all_in_one.domain.AllInOneConstructor;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.all_in_one.AllInOneConstructorProto;
import org.silbertb.proto.domainconverter.test.proto.all_in_one.AllInOneProto;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AllInOneTest {

    @Test
    void testAllInOneToProto() {
        AllInOne domain = AllInOneTestObjectsCreator.createAllInOneDomain();
        AllInOneProto proto = ProtoDomainConverter.toProto(domain);
        AllInOneProto expected = AllInOneTestObjectsCreator.createAllInOneProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneToDomain() {
        AllInOneProto proto = AllInOneTestObjectsCreator.createAllInOneProto();
        AllInOne domain = ProtoDomainConverter.toDomain(proto);
        AllInOne expected = AllInOneTestObjectsCreator.createAllInOneDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testAllInOneConstructorToProto() {
        AllInOneConstructor domain = AllInOneTestObjectsCreator.createAllInOneConstructorDomain();
        AllInOneConstructorProto proto = ProtoDomainConverter.toProto(domain);
        AllInOneConstructorProto expected = AllInOneTestObjectsCreator.createAllInOneConstructorProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneConstructorToDomain() {
        AllInOneConstructorProto proto = AllInOneTestObjectsCreator.createAllInOneConstructorProto();
        AllInOneConstructor domain = ProtoDomainConverter.toDomain(proto);
        AllInOneConstructor expected = AllInOneTestObjectsCreator.createAllInOneConstructorDomain();

        assertEquals(expected, domain);
    }
}
