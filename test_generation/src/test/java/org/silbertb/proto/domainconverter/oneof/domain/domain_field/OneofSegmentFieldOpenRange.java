package org.silbertb.proto.domainconverter.oneof.domain.domain_field;

import lombok.Builder;
import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.oneof.domain.RangeDomain;

@ProtoBuilder
@Data
@Builder
public class OneofSegmentFieldOpenRange implements OneofSegmentField {
    @ProtoField(protoName = "open_range")
    private RangeDomain range;
}
