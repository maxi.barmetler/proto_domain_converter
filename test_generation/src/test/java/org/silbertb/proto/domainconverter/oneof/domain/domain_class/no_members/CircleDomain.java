package org.silbertb.proto.domainconverter.oneof.domain.domain_class.no_members;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.oneof.CircleProto;

@ProtoClass(protoClass = CircleProto.class)
@EqualsAndHashCode
@Getter
public class CircleDomain implements ShapeDomain {
    private final double radius;

    @ProtoConstructor
    public CircleDomain(@ProtoField double radius) {
        this.radius = radius;
    }

}
