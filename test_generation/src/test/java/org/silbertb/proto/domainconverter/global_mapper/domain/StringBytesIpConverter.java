package org.silbertb.proto.domainconverter.global_mapper.domain;

import com.google.protobuf.ByteString;
import org.silbertb.proto.domainconverter.custom.TypeConverter;

import java.net.Inet4Address;
import java.net.UnknownHostException;

public class StringBytesIpConverter implements TypeConverter<String, ByteString> {
    @Override
    public String toDomainValue(ByteString protoValue) {
        try {
            return Inet4Address.getByAddress(protoValue.toByteArray()).toString();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean shouldAssignToProto(String domainValue) {
        return false;
    }

    @Override
    public ByteString toProtobufValue(String domainValue) {
        try {
            return ByteString.copyFrom(Inet4Address.getByName(domainValue).getAddress());
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}
