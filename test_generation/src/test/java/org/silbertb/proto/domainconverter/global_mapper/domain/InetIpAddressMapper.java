package org.silbertb.proto.domainconverter.global_mapper.domain;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.test.proto.IpAddressProto;

import java.net.Inet4Address;

public interface InetIpAddressMapper extends Mapper<Inet4Address, IpAddressProto>  {
}
