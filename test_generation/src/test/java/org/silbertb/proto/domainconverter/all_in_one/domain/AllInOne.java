package org.silbertb.proto.domainconverter.all_in_one.domain;

import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.collection.domain.ConcreteMapToMessage;
import org.silbertb.proto.domainconverter.collection.domain.MessageListDomain;
import org.silbertb.proto.domainconverter.basic.domain.BytesDomain;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.basic.domain.StringDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofBaseFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.all_in_one.AllInOneProto;

@Data
@ProtoClass(protoClass = AllInOneProto.class)
public class AllInOne {
    @ProtoField
    private StringDomain strVal;

    @ProtoField
    private BytesDomain bytesVal;

    @ProtoField
    private ConcreteMapToMessage mapVal;

    @ProtoField
    private MessageListDomain listVal;

    @OneofBase(oneOfFields = {
            @OneofField(protoField = "oneof1_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
            @OneofField(protoField = "oneof1_primitives", domainClass = PrimitiveDomain.class)
    })
    private OneofBaseFieldDomain value1;

    @OneofBase(oneOfFields = {
            @OneofField(protoField = "oneof2_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
            @OneofField(protoField = "oneof2_primitives", domainClass = PrimitiveDomain.class)
    })
    private OneofBaseFieldDomain value2;
}