package org.silbertb.proto.domainconverter.oneof.domain.domain_class.class_builder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;

@ProtoClassMapper(mapper = SegmentBuilderRangeMapper.class)
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class OneofSegmentBuilderRange extends OneofSegmentBuilderDomain {
    private final int start;
    private final int end;
}
