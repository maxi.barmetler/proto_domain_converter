package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoConfigure;
import org.silbertb.proto.domainconverter.annotations.ProtoImport;
import org.silbertb.proto.domainconverter.conversion_data.ConfigurationData;
import org.silbertb.proto.domainconverter.util.LangModelUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ConfigurationCreator {

    private final ProcessingEnvironment processingEnv;
    private final LangModelUtil langModelUtil;

    public ConfigurationCreator(ProcessingEnvironment processingEnv, LangModelUtil langModelUtil) {
        this.processingEnv = processingEnv;
        this.langModelUtil = langModelUtil;
    }

    public ConfigurationData createConfigurationData(RoundEnvironment roundEnv) {
        Set<? extends Element> configurationClasses = roundEnv.getElementsAnnotatedWith(ProtoConfigure.class);
        if(configurationClasses.size() > 1) {
            String configurationClassNames = configurationClasses.stream()
                    .map(e -> ((TypeElement)e).getQualifiedName().toString())
                    .collect(Collectors.joining(", "));
            throw new IllegalArgumentException("More than one configuration class: " + configurationClassNames);
        }

        if(configurationClasses.isEmpty()) {
            return new ConfigurationData(
                    defaultConverterName(),
                    new HashMap<>(),
                    Set.of(),
                    Set.of());
        }

        Element configurationClass = configurationClasses.iterator().next();
        return createConfigurationData((TypeElement) configurationClass);
    }

    private ConfigurationData createConfigurationData(TypeElement configurationClass) {
        ProtoConfigure protoConfigureAnnotation = configurationClass.getAnnotation(ProtoConfigure.class);
        String converterName = getConverterName(protoConfigureAnnotation);

        Map<String, String> domainClassToConverter = new HashMap<>();
        Set<String> directImports = new HashSet<>();
        Set<String> transitiveImports = new HashSet<>();
        processImports(configurationClass, domainClassToConverter, directImports, transitiveImports);

        return new ConfigurationData(
                converterName,
                domainClassToConverter,
                directImports,
                transitiveImports);
    }

    private String defaultConverterName() {
        String converterName = processingEnv.getOptions().get("proto.domain.converter.name");
        if(converterName == null) {
            return "org.silbertb.proto.domainconverter.generated.ProtoDomainConverter";
        }

        return converterName;
    }
    private String getConverterName(ProtoConfigure protoConfigureAnnotation) {
        String converterName = protoConfigureAnnotation.converterName();
        if(converterName.isEmpty()) {
            return defaultConverterName();
        }

        return converterName;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void processImports(TypeElement configurationClass,
                                Map<String, String> domainClassToConverter,
                                Set<String> directImports,
                                Set<String> transitiveImports) {
        ProtoImport[] protoImportAnnotations = configurationClass.getAnnotationsByType(ProtoImport.class);
        for(ProtoImport protoImport : protoImportAnnotations) {
            TypeMirror importedConverter = langModelUtil.getClassFromAnnotation(protoImport::value);
            boolean transitiveImport = protoImport.transitive();
            if(transitiveImport) {
                updateTransitiveImports(importedConverter, domainClassToConverter);
                transitiveImports.add(importedConverter.toString());
            } else {
                updateDomainClassToConverter(importedConverter, domainClassToConverter);
                directImports.add(importedConverter.toString());
            }
        }
    }

    private void updateDomainClassToConverter(TypeMirror importClass, Map<String, String> domainClassToConverter) {
        TypeElement importElement = (TypeElement)processingEnv.getTypeUtils().asElement(importClass);
        langModelUtil.getAllMethods(importElement).stream()
                .filter(method -> method.getSimpleName().toString().equals("toDomain"))
                .forEach(method -> {
                    String converterName = importClass.toString();
                    domainClassToConverter.put(method.getReturnType().toString(), converterName);
                });
    }

    private void updateTransitiveImports(TypeMirror configurationConverter, Map<String, String> domainClassToConverter) {
        updateDomainClassToConverter(configurationConverter, domainClassToConverter);

        TypeElement configurationConverterElement = (TypeElement)processingEnv.getTypeUtils().asElement(configurationConverter);

        //update direct imports
        langModelUtil.getAllMethods(configurationConverterElement).stream()
                .filter(method -> method.getSimpleName().toString().equals("directImportConverterMarker"))
                .forEach(method -> updateDomainClassToConverter(method.getParameters().get(0).asType(), domainClassToConverter));

        //update transitive imports
        langModelUtil.getAllMethods(configurationConverterElement).stream()
                .filter(method -> method.getSimpleName().toString().equals("transitiveConfigurationConverterMarker"))
                .forEach(method -> updateTransitiveImports(method.getParameters().get(0).asType(), domainClassToConverter));
    }


}
