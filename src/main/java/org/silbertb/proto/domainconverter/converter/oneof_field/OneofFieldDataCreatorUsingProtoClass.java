package org.silbertb.proto.domainconverter.converter.oneof_field;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.conversion_data.OneofFieldData;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.lang.model.type.TypeMirror;

public class OneofFieldDataCreatorUsingProtoClass {
    private final ProtoTypeUtil protoTypeUtil;
    private final OneofConverterUtil oneofConverterUtil;


    public OneofFieldDataCreatorUsingProtoClass(ProtoTypeUtil protoTypeUtil, OneofConverterUtil oneofConverterUtil) {
        this.protoTypeUtil = protoTypeUtil;
        this.oneofConverterUtil = oneofConverterUtil;
    }

    public OneofFieldData create(OneofField oneofFieldAnnotation, TypeMirror domainType) {
        boolean domainTypeIsMessage = protoTypeUtil.isProtoMessage(domainType);
        if(!domainTypeIsMessage ||
                !oneofFieldAnnotation.domainField().isEmpty() ||
                !oneofConverterUtil.isNullConverter(oneofFieldAnnotation)) {
            return null;
        }

        return OneofFieldData.builder()
                .protoField(oneofFieldAnnotation.protoField())
                .oneofImplClass(domainType.toString())
                .build();
    }
}
