package org.silbertb.proto.domainconverter.converter;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic;

public class ConverterLogger {

    private final ProcessingEnvironment processingEnv;

    public ConverterLogger(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    public void info(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
    }

    public void error(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, msg);
    }
}
