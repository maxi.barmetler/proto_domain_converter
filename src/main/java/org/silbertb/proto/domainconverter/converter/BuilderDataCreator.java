package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.conversion_data.BuilderData;
import org.silbertb.proto.domainconverter.conversion_data.FieldData;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import java.util.List;
import java.util.stream.Collectors;

public class BuilderDataCreator {

    private final ConstructorParametersDataCreator constructorParametersDataCreator;

    public BuilderDataCreator(ConstructorParametersDataCreator constructorParametersDataCreator) {
        this.constructorParametersDataCreator = constructorParametersDataCreator;
    }

    public BuilderData createClassLevelBuilderData(TypeElement domainElement) {
        ProtoBuilder protoBuilderAnnotation = domainElement.getAnnotation(ProtoBuilder.class);
        return protoBuilderAnnotation == null ? null : createBuilderData(protoBuilderAnnotation, false);
    }

    public BuilderData getConstructorBuilderData(final TypeElement domainElement) {
        List<Element> constructors = domainElement.getEnclosedElements().stream()
                .filter(e -> e.getKind().equals(ElementKind.CONSTRUCTOR))
                .collect(Collectors.toList());
        for(Element constructor : constructors) {
            ProtoBuilder protoBuilderAnnotation = constructor.getAnnotation(ProtoBuilder.class);
            if(protoBuilderAnnotation != null) {
                return createBuilderData(protoBuilderAnnotation, true);
            }
        }

        return null;
    }

    public List<FieldData> createBuilderConstructorParametersData(final TypeElement domainElement) {
        return constructorParametersDataCreator.getConstructorParametersData(domainElement, ProtoBuilder.class);
    }

    private BuilderData createBuilderData(ProtoBuilder protoBuilderAnnotation, boolean constructorAnnotation) {
        return BuilderData.builder()
                .builderMethodName(protoBuilderAnnotation.builderMethodName())
                .buildMethodName(protoBuilderAnnotation.buildMethodName())
                .setterPrefix(protoBuilderAnnotation.setterPrefix())
                .useConstructorParams(constructorAnnotation)
                .build();
    }
}
