package org.silbertb.proto.domainconverter.converter.oneof_field;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.conversion_data.OneofFieldData;

import javax.lang.model.element.TypeElement;

public class OneofFieldDataCreatorUsingClassConverter {

    private final OneofConverterUtil oneofConverterUtil;

    public OneofFieldDataCreatorUsingClassConverter(OneofConverterUtil oneofConverterUtil) {
        this.oneofConverterUtil = oneofConverterUtil;
    }

    public OneofFieldData create(OneofField oneofFieldAnnotation, TypeElement domainElement) {
        if(!oneofFieldAnnotation.domainField().isEmpty()) {
            //mapping to a field within the domain class
            return null;
        }

        String converterFullName = oneofConverterUtil.getTypeConverterName(oneofFieldAnnotation, domainElement);
        if(converterFullName == null) {
            return null;
        }

        return OneofFieldData.builder()
                .protoField(oneofFieldAnnotation.protoField())
                .oneofImplClass(domainElement.getQualifiedName().toString())
                .converterFullName(converterFullName)
                .build();
    }
}
