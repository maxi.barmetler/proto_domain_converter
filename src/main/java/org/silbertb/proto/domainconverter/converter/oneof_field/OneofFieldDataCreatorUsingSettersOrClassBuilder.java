package org.silbertb.proto.domainconverter.converter.oneof_field;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.conversion_data.*;
import org.silbertb.proto.domainconverter.converter.BuilderDataCreator;
import org.silbertb.proto.domainconverter.converter.FieldDataCreator;
import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;
import org.silbertb.proto.domainconverter.util.StringUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.stream.Collectors;

public class OneofFieldDataCreatorUsingSettersOrClassBuilder {
    private final ProcessingEnvironment processingEnv;
    private final LangModelUtil langModelUtil;
    private final ProtoTypeUtil protoTypeUtil;
    private final BuilderDataCreator builderDataCreator;
    private final FieldDataCreator fieldDataCreator;
    private final OneofConverterUtil oneofConverterUtil;

    public OneofFieldDataCreatorUsingSettersOrClassBuilder(ProcessingEnvironment processingEnv, LangModelUtil langModelUtil, ProtoTypeUtil protoTypeUtil, BuilderDataCreator builderDataCreator, FieldDataCreator fieldDataCreator, OneofConverterUtil oneofConverterUtil) {
        this.processingEnv = processingEnv;
        this.langModelUtil = langModelUtil;
        this.protoTypeUtil = protoTypeUtil;
        this.builderDataCreator = builderDataCreator;
        this.fieldDataCreator = fieldDataCreator;
        this.oneofConverterUtil = oneofConverterUtil;
    }

    public OneofFieldData create(OneofField oneofFieldAnnotation, TypeElement domainTypeElement, String oneofBaseField) {
        BuilderData classBuilderData = builderDataCreator.createClassLevelBuilderData(domainTypeElement);
        ConcreteFieldData concreteFieldData = getConcreteField(oneofFieldAnnotation, oneofBaseField);
        if(concreteFieldData == null) {
            return null;
        }
        return OneofFieldData.builder()
                .protoField(oneofFieldAnnotation.protoField())
                .oneofImplClass(domainTypeElement.getQualifiedName().toString())
                .fieldData(concreteFieldData)
                .builderData(classBuilderData)
                .build();
    }

    private ConcreteFieldData getConcreteField(OneofField oneofFieldAnnotation, String oneofBaseField) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror domainType = langModelUtil.getClassFromAnnotation(oneofFieldAnnotation::domainClass);

        ConcreteFieldData concreteFieldDataUsingDomainField = getConcreteFieldUsingDomainField(oneofFieldAnnotation, oneofBaseField, domainType);
        ConcreteFieldData concreteFieldDataUsingProtoFieldName = getConcreteFieldUsingProtoFieldName(oneofFieldAnnotation, domainType);
        ConcreteFieldData concreteFieldDataUsingProtoFieldAnnotation = getConcreteFieldUsingProtoFieldAnnotation(oneofFieldAnnotation, domainType);

        if(concreteFieldDataUsingDomainField != null) {
            return concreteFieldDataUsingDomainField;
        }

        if(concreteFieldDataUsingProtoFieldAnnotation != null) {
            return concreteFieldDataUsingProtoFieldAnnotation;
        }

        return concreteFieldDataUsingProtoFieldName;
    }

    private ConcreteFieldData getConcreteFieldUsingDomainField(OneofField oneofFieldAnnotation, String oneofBaseField, TypeMirror domainType) {
        VariableElement fieldElement = getFieldElementUsingDomainField(oneofFieldAnnotation, domainType, oneofBaseField);
        if(fieldElement == null) {
            return null;
        }

        return getConcreteFieldUsingDomainField(fieldElement, oneofFieldAnnotation);
    }

    private ConcreteFieldData getConcreteFieldUsingProtoFieldName(OneofField oneofFieldAnnotation, TypeMirror domainType) {
        String protoFieldName = StringUtils.snakeCaseToCamelCase(oneofFieldAnnotation.protoField());
        VariableElement fieldElement = langModelUtil.getMemberField(domainType, protoFieldName);
        if(fieldElement == null) {
            return null;
        }
        return getConcreteFieldUsingDomainField(fieldElement, oneofFieldAnnotation);
    }

    private ConcreteFieldData getConcreteFieldUsingProtoFieldAnnotation(OneofField oneofFieldAnnotation, TypeMirror domainType) {
        TypeElement domainElement = (TypeElement) processingEnv.getTypeUtils().asElement(domainType);
        List<FieldData> fieldsData = fieldDataCreator.getFieldData(domainElement, true, false);
        if(fieldsData.isEmpty()) {
            String protoFieldPascalCase = StringUtils.snakeCaseToPascalCase(oneofFieldAnnotation.protoField());
            fieldsData = fieldDataCreator.getFieldData(domainElement, true, true).stream()
                    .filter(f -> f.concreteFieldData() != null && f.concreteFieldData().protoFieldPascalCase().equals(protoFieldPascalCase))
                    .collect(Collectors.toList());
        }
        return getConcreteField(fieldsData, domainElement, oneofFieldAnnotation);
    }

    private VariableElement getFieldElementUsingDomainField(OneofField oneofFieldAnnotation, TypeMirror domainType, String oneofBaseField) {
        if(oneofFieldAnnotation.domainField().isEmpty()) {
            return null;
        }

        VariableElement fieldElement = langModelUtil.getMemberField(domainType, oneofFieldAnnotation.domainField());
        if(fieldElement == null) {
            throw new RuntimeException("OneofField annotation on field '" + oneofBaseField +
                    "' declared non-existing field for class '" + domainType + "': " +
                    oneofFieldAnnotation.domainField());
        }
        return fieldElement;
    }

    private ConcreteFieldData getConcreteFieldUsingDomainField(VariableElement fieldElement , OneofField oneofFieldAnnotation) {
        String converterFullName = getFieldConverterName(fieldElement, oneofFieldAnnotation);

        ProtoType protoTypeForConverter = converterFullName == null ? null : ProtoType.OTHER;
        String oneofDomainField = oneofFieldAnnotation.domainField();
        String oneofDomainFieldType = fieldElement.asType().toString();
        ConcreteFieldData.ConcreteFieldDataBuilder fieldData = ConcreteFieldData.builder();
        FieldType fieldType = protoTypeUtil.calculateFieldType(fieldElement.asType());
        TypeMirror elementType = protoTypeUtil.getElementType(fieldElement.asType(), fieldType);

        return fieldData.domainFieldName(oneofDomainField)
                .domainTypeFullName(oneofDomainFieldType)
                .explicitProtoFieldName(oneofFieldAnnotation.protoField())
                .fieldType(fieldType)
                .domainItemTypeFullName(elementType == null ? null : elementType.toString())
                .dataStructureConcreteType(protoTypeUtil.calculateDataStructureConcreteType(fieldElement))
                .converterFullName(converterFullName)
                .protoTypeForConverter(protoTypeForConverter)
                .build();
    }

    private String getFieldConverterName(VariableElement fieldElement, OneofField oneofFieldAnnotation) {
        return oneofConverterUtil.getTypeConverterName(oneofFieldAnnotation, fieldElement);
    }

    private ConcreteFieldData getConcreteField(List<FieldData> fieldsData, TypeElement domainElement, OneofField oneofFieldAnnotation) {
        if(fieldsData.isEmpty()) {
            return null;
        }

        if(fieldsData.size() != 1) {
            throw new RuntimeException("There should be only one class member for " + domainElement);
        }

        FieldData param = fieldsData.get(0);
        if(param.oneofFieldData() != null) {
            throw new RuntimeException("There shouldn't be class member for " + domainElement + " annotated with @OneofBase since this class is oneof implementation itself.");
        }

        if(!param.concreteFieldData().protoFieldPascalCase().equals(
                StringUtils.snakeCaseToPascalCase(oneofFieldAnnotation.protoField()))) {
            throw new RuntimeException("class member for " + domainElement + " should correlate proto field " + oneofFieldAnnotation.protoField());
        }

        return param.concreteFieldData();
    }
}
