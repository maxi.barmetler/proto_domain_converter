package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.conversion_data.BuilderData;
import org.silbertb.proto.domainconverter.conversion_data.ClassData;
import org.silbertb.proto.domainconverter.conversion_data.ConfigurationData;
import org.silbertb.proto.domainconverter.conversion_data.FieldData;
import org.silbertb.proto.domainconverter.custom.NullMapper;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.*;

public class ClassDataCreator {
    private final LangModelUtil langModelUtil;
    private final OneofBaseDataCreator oneofBaseDataCreator;
    private final ConstructorParametersDataCreator constructorParametersDataCreator;
    private final BuilderDataCreator builderDataCreator;
    private final FieldDataCreator fieldDataCreator;

    public ClassDataCreator(LangModelUtil langModelUtil,
                            ProcessingEnvironment processingEnv,
                            ProtoTypeUtil protoTypeUtil,
                            ConverterLogger logger,
                            ConfigurationData configurationData,
                            Set<String> domainFieldsWithGlobalMappers) {
        this.langModelUtil = langModelUtil;
        ConcreteFieldDataCreator concreteFieldDataCreator =
                new ConcreteFieldDataCreator(langModelUtil, protoTypeUtil, logger, configurationData, domainFieldsWithGlobalMappers);
        this.oneofBaseDataCreator = new OneofBaseDataCreator(langModelUtil, protoTypeUtil, processingEnv, logger, this);
        this.constructorParametersDataCreator = new ConstructorParametersDataCreator(concreteFieldDataCreator, oneofBaseDataCreator);
        this.builderDataCreator = new BuilderDataCreator(constructorParametersDataCreator);
        this.fieldDataCreator = new FieldDataCreator(processingEnv, concreteFieldDataCreator, oneofBaseDataCreator);
        oneofBaseDataCreator.setConstructorParametersDataCreator(constructorParametersDataCreator);
        oneofBaseDataCreator.setBuilderDataCreator(builderDataCreator);
        oneofBaseDataCreator.setFieldDataCreator(fieldDataCreator);
    }

    public ClassData createClassData(TypeElement domainElement) {
        ProtoClass protoClassAnnotation = domainElement.getAnnotation(ProtoClass.class);

        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror protoClass = langModelUtil.getClassFromAnnotation(protoClassAnnotation::protoClass);

        //deprecated way to use mapper
        String mapperFullName = getCustomMapper(protoClassAnnotation);

        boolean withInheritedFields = protoClassAnnotation.withInheritedFields();

        return createClassData(domainElement, protoClass, withInheritedFields, mapperFullName, protoClassAnnotation.blacklist());
    }

    public ClassData createClassData(GlobalMapperInfo globalMapperTypes) {
        return ClassData.builder()
                .domainFullName(globalMapperTypes.getDomainName())
                .protoFullName(globalMapperTypes.getProtoName())
                .mapperFullName(globalMapperTypes.getMapperName())
                .isDefault(globalMapperTypes.isDefault())
                .build();
    }

    public ClassData createClassData(TypeElement domainElement, TypeMirror protoClass, boolean withInheritedFields, boolean blacklist) {
        return createClassData(domainElement, protoClass, withInheritedFields, null, blacklist);
    }

    private ClassData createClassData(TypeElement domainElement, TypeMirror protoClass,
                                     boolean withInheritedFields,
                                     String mapperFullNameFromProtoClassAnnotation,
                                      boolean blacklist) {
        ClassData.ClassDataBuilder classDataBuilder = ClassData.builder()
                .domainFullName(domainElement.getQualifiedName().toString())
                .protoFullName(protoClass.toString());

        String mapperFullName = null;
        if(mapperFullNameFromProtoClassAnnotation == null) {
            //recommended way to use mapper
            ProtoClassMapper mapperAnnotation = domainElement.getAnnotation(ProtoClassMapper.class);
            if(mapperAnnotation != null) {
                mapperFullName = getCustomMapper(mapperAnnotation);
            }
        }

        BuilderData builderData = builderDataCreator.createClassLevelBuilderData(domainElement);

        List<FieldData> constructorParametersData =
                constructorParametersDataCreator.getConstructorParametersData(domainElement, ProtoConstructor.class, Modifier.PUBLIC);
        List<FieldData> builderConstructorParametersData = builderDataCreator.createBuilderConstructorParametersData(domainElement);

        validateAnnotations(domainElement, mapperFullName, builderData, constructorParametersData, builderConstructorParametersData);

        if(mapperFullName != null) {
            return classDataBuilder
                    .mapperFullName(mapperFullName)
                    .constructorParameters(Collections.emptyList())
                    .fieldsData(Collections.emptyList())
                    .build();
        }

        ArrayList<FieldData> fieldsData = fieldDataCreator.getFieldData(domainElement, withInheritedFields, blacklist);
        if(constructorParametersData != null) {
            classDataBuilder.constructorParameters(constructorParametersData);
            fieldsData = removeFormFieldData(fieldsData, constructorParametersData);
        }

        if(builderConstructorParametersData != null){
            classDataBuilder.constructorParameters(builderConstructorParametersData);
            builderData = builderDataCreator.getConstructorBuilderData(domainElement);
            fieldsData = removeFormFieldData(fieldsData, builderConstructorParametersData);
        }

        classDataBuilder.builderData(builderData);

        classDataBuilder.fieldsData(fieldsData);

        OneofBase oneofBaseAnnotation = domainElement.getAnnotation(OneofBase.class);
        classDataBuilder.oneofBaseClassData(oneofBaseDataCreator.createOneofBaseClassData(oneofBaseAnnotation, protoClass, blacklist));

        ProtoClassDefault protoClassDefaultAnnotation = domainElement.getAnnotation(ProtoClassDefault.class);
        if(protoClassDefaultAnnotation != null) {
            classDataBuilder.isDefault(true);
        }

        return classDataBuilder.build();
    }

    private ArrayList<FieldData> removeFormFieldData(List<FieldData> fieldDataList, List<FieldData> fieldsToRemove) {
        LinkedHashMap<String, FieldData> fieldDataMap = new LinkedHashMap<>();
        for(FieldData fieldData : fieldDataList) {
            String domainFieldName = getDomainFieldName(fieldData);
            fieldDataMap.put(domainFieldName, fieldData);
        }

        for (FieldData fieldData : fieldsToRemove) {
            String domainFieldName = getDomainFieldName(fieldData);
            fieldDataMap.remove(domainFieldName);
        }

        ArrayList<FieldData> remainingFields = new ArrayList<>(fieldDataMap.values());
        if(!remainingFields.isEmpty()) {
            FieldData last = remainingFields.get(remainingFields.size()-1);
            FieldData newLast = FieldData.builder()
                    .concreteFieldData(last.concreteFieldData())
                    .oneofFieldData(last.oneofFieldData())
                    .isLast(true)
                    .build();
            remainingFields.set(remainingFields.size()-1, newLast);
        }

        return remainingFields;
    }

    private String getDomainFieldName(FieldData fieldData) {
        return fieldData.concreteFieldData() == null ?
                fieldData.oneofFieldData().domainFieldName() :
                fieldData.concreteFieldData().domainFieldName();
    }

    private void validateAnnotations(TypeElement domainElement,
                                     String mapperFullName,
                                     BuilderData classLevelBuilderData,
                                     List<FieldData> constructorParametersData,
                                     List<FieldData> builderConstructorParametersData) {
        if(mapperFullName != null) {
            if(classLevelBuilderData != null || builderConstructorParametersData != null) {
                throw new IllegalArgumentException("Both annotations are used:  @ProtoClassMapper and @ProtoBuilder. class: " + domainElement);
            }

            if(constructorParametersData != null) {
                throw new IllegalArgumentException("Both annotations are used:  @ProtoClassMapper and @ProtoConstructor. class: " + domainElement);
            }
        }

        if(constructorParametersData != null && builderConstructorParametersData != null) {
            throw new IllegalArgumentException("Both annotations are used on constructors:  @ProtoConstructor and @ProtoBuilder. class: " + domainElement);
        }

        if(classLevelBuilderData != null && builderConstructorParametersData != null) {
            throw new IllegalArgumentException("@ProtoBuilder annotation is used both in class context and in constructor context. class: " + domainElement);
        }
    }

    private String getCustomMapper(ProtoClass protoClassAnnotation) {
        @SuppressWarnings({"ResultOfMethodCallIgnored", "deprecation"})
        TypeMirror mapperClass = langModelUtil.getClassFromAnnotation(protoClassAnnotation::mapper);
        String mapperFullName = mapperClass.toString();

        if(mapperFullName.equals(NullMapper.class.getName())) {
            return null;
        }

        return mapperFullName;
    }

    private String getCustomMapper(ProtoClassMapper protoMapperAnnotation) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror mapperClass = langModelUtil.getClassFromAnnotation(protoMapperAnnotation::mapper);
        return mapperClass.toString();

    }
}
