package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.annotations.ProtoGlobalMapper;
import org.silbertb.proto.domainconverter.conversion_data.ClassData;
import org.silbertb.proto.domainconverter.conversion_data.ConfigurationData;
import org.silbertb.proto.domainconverter.conversion_data.ConversionData;
import org.silbertb.proto.domainconverter.conversion_data.EnumData;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConversionDataCreator {

    private final ProcessingEnvironment processingEnv;
    private final LangModelUtil langModelUtil;
    private final ConverterLogger logger;

    public ConversionDataCreator(ProcessingEnvironment processingEnv,
                                 ConverterLogger logger) {
        this.processingEnv = processingEnv;
        this.logger = logger;
        this.langModelUtil = new LangModelUtil(processingEnv);
    }

    public ConversionData createConversionData(RoundEnvironment roundEnv) {
        LangModelUtil langModelUtil = new LangModelUtil(processingEnv);
        ProtoTypeUtil protoTypeUtil = new ProtoTypeUtil(processingEnv, langModelUtil);

        ConfigurationCreator configurationCreator = new ConfigurationCreator(processingEnv, langModelUtil);
        ConfigurationData configurationData = configurationCreator.createConfigurationData(roundEnv);
        List<GlobalMapperInfo> globalMappers = findGlobalMappers(roundEnv);
        List<GlobalMapperInfo> predefinedGlobalMappers = getPredefinedGlobalMappers();

        Set<String> domainFieldsWithGlobalMappers =
                Stream.concat(globalMappers.stream(), predefinedGlobalMappers.stream())
                        .map(GlobalMapperInfo::getDomainName)
                        .collect(Collectors.toSet());
        domainFieldsWithGlobalMappers.forEach(domainField -> configurationData.domainClassToConverter().remove(domainField));

        ClassDataCreator classDataCreator = new ClassDataCreator(
                langModelUtil,
                processingEnv,
                protoTypeUtil,
                logger,
                configurationData,
                domainFieldsWithGlobalMappers
        );

        ConversionData.ConversionDataBuilder conversionData = ConversionData.builder()
                .generator(this.getClass().getName())
                .converterFullName(configurationData.generatedConverterName());

        List<EnumData> enumDataList = createEnumsData(roundEnv);
        Map<String, List<ClassData>> protoToDomainClass = createClassesDataFromProtoClass(roundEnv, classDataCreator);
        Map<String, List<ClassData>> protoToDomainClassFromGlobalMappers = createClassesData(globalMappers, classDataCreator);
        mergeClassDataMap(protoToDomainClass, protoToDomainClassFromGlobalMappers, false);

        Map<String, List<ClassData>> protoToDomainClassFromPredefinedGlobalMappers = createClassesData(predefinedGlobalMappers, classDataCreator);
        mergeClassDataMap(protoToDomainClass, protoToDomainClassFromPredefinedGlobalMappers, true);

        List<ClassData> defaultDomainClasses = getDefaultDomainClasses(protoToDomainClass);

        return conversionData
                .configurationData(configurationData)
                .classesData(protoToDomainClass.values().stream().flatMap(Collection::stream).collect(Collectors.toUnmodifiableList()))
                .defaultClassesData(defaultDomainClasses)
                .enumData(enumDataList)
                .build();
    }

    private List<GlobalMapperInfo> getPredefinedGlobalMappers() {
        return getPredefinedGlobalMappers("org.silbertb.proto.domainconverter.predefined_mappers.InstantTimestampMapper");
    }

    private List<GlobalMapperInfo> getPredefinedGlobalMappers(String... classNames) {
        GlobalMapperTypesCreator creator = new GlobalMapperTypesCreator(langModelUtil);
        return Arrays.stream(classNames)
                .map(className -> processingEnv.getElementUtils().getTypeElement(className))
                .map(creator::create)
                .collect(Collectors.toList());
    }

    private void mergeClassDataMap(Map<String, List<ClassData>> classDataFromProtoClass,
                                   Map<String, List<ClassData>> classDataFromGlobalMapper,
                                   boolean ignoreIfExists) {
        for (Map.Entry<String, List<ClassData>> e : classDataFromGlobalMapper.entrySet()) {
            List<ClassData> merged = mergeClassDataLists(classDataFromProtoClass.get(e.getKey()), e.getValue(), ignoreIfExists);
            classDataFromProtoClass.put(e.getKey(), merged);
        }
    }

    private List<ClassData> mergeClassDataLists(List<ClassData> l1, List<ClassData> l2, boolean ignoreIfExists) {
        Map<String, ClassData> domainClassToClassData = new HashMap<>();
        if (l1 != null) {
            for (ClassData classData : l1) {
                domainClassToClassData.put(classData.domainFullName(), classData);
            }
        }

        if (l2 != null) {
            for (ClassData classData : l2) {
                if (domainClassToClassData.containsKey(classData.domainFullName())) {
                    if (!ignoreIfExists) {
                        throw new IllegalArgumentException("Both @ProtoClass and @ProtoGlobalMapper are defined to the same proto message and domain class. proto: "
                                + classData.protoFullName() + " domain: " + classData.domainFullName());
                    }
                } else {
                    domainClassToClassData.put(classData.domainFullName(), classData);
                }
            }
        }

        return new ArrayList<>(domainClassToClassData.values());
    }

    private List<GlobalMapperInfo> findGlobalMappers(RoundEnvironment roundEnv) {
        GlobalMapperTypesCreator creator = new GlobalMapperTypesCreator(langModelUtil);
        return roundEnv.getElementsAnnotatedWith(ProtoGlobalMapper.class).stream()
                .map(e -> creator.create((TypeElement) e))
                .collect(Collectors.toList());
    }

    private Map<String, List<ClassData>> createClassesData(List<GlobalMapperInfo> globalMappersInfo, ClassDataCreator classDataCreator) {
        Map<String, List<ClassData>> protoToDomainClass = new HashMap<>();
        for (GlobalMapperInfo globalMapperInfo : globalMappersInfo) {
            ClassData classData = classDataCreator.createClassData(globalMapperInfo);
            protoToDomainClass.computeIfAbsent(classData.protoFullName(), k -> new ArrayList<>()).add(classData);
        }
        return protoToDomainClass;
    }

    private Map<String, List<ClassData>> createClassesDataFromProtoClass(RoundEnvironment roundEnv, ClassDataCreator classDataCreator) {
        Map<String, List<ClassData>> protoToDomainClass = new HashMap<>();
        for (Element domainElement : roundEnv.getElementsAnnotatedWith(ProtoClass.class)) {
            ClassData classData = classDataCreator.createClassData((TypeElement) domainElement);
            protoToDomainClass.computeIfAbsent(classData.protoFullName(), k -> new ArrayList<>()).add(classData);
        }
        return protoToDomainClass;
    }

    private List<ClassData> getDefaultDomainClasses(Map<String, List<ClassData>> protoToDomainClass) {
        Function<List<ClassData>, Optional<ClassData>> defaultDomainClassExtractor = e -> {
            if (e.size() == 1) {
                return Optional.of(e.get(0));
            }
            List<ClassData> defaults = e.stream().filter(ClassData::isDefault).collect(Collectors.toUnmodifiableList());
            if (defaults.size() > 1) {
                throw new IllegalArgumentException("More than one default class exists for proto '" + e.get(0).domainFullName() + "'.");
            } else if (defaults.size() == 1) {
                return Optional.of(defaults.get(0));
            }
            // More than one domain class, but none are marked as default.
            return Optional.empty();
        };
        return protoToDomainClass.values().stream()
                .map(defaultDomainClassExtractor)
                .flatMap(Optional::stream)
                .collect(Collectors.toUnmodifiableList());
    }

    private List<EnumData> createEnumsData(RoundEnvironment roundEnv) {
        EnumDataCreator enumDataCreator = new EnumDataCreator(langModelUtil, processingEnv, logger);
        List<EnumData> enumDataList = new ArrayList<>();
        for (Element domainElement : roundEnv.getElementsAnnotatedWith(ProtoEnum.class)) {
            EnumData enumData = enumDataCreator.createEnumData((TypeElement) domainElement);
            enumDataList.add(enumData);
        }
        return enumDataList;
    }
}
