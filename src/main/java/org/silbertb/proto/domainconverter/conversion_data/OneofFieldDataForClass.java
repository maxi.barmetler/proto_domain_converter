package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

@Accessors(fluent = true)
@Builder
@Data
public class OneofFieldDataForClass {
    private final String protoField;
    private final boolean isMessage;
    private final ClassData classData;

    public String oneofImplClass() {
        return classData.domainFullName();
    }

    public String oneOfProtoField() {
        return StringUtils.snakeCaseToPascalCase(protoField);
    }

    public String oneofFieldCase() {
        return protoField.toUpperCase();
    }
}
