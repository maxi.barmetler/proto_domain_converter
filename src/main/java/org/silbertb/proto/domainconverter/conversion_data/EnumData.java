package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

import java.util.Collection;

@Accessors(fluent = true)
@Value
public class EnumData {
    String domainFullName;
    String protoFullName;
    Collection<EnumValueData> enumValueData;

    String domainPackage;
    String protoPackage;

    @Builder
    EnumData(String domainFullName,
             String protoFullName,
             Collection<EnumValueData> enumValueData) {
        this.domainFullName = domainFullName;
        this.protoFullName = protoFullName;
        this.enumValueData = enumValueData;

        this.domainPackage = StringUtils.getPackage(domainFullName);
        this.protoPackage = StringUtils.getPackage(protoFullName);
    }
}
