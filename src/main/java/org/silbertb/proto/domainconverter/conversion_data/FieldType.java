package org.silbertb.proto.domainconverter.conversion_data;

public enum FieldType {
    MESSAGE,
    BOOLEAN,
    STRING,
    BYTES,
    PRIMITIVE_LIST,
    MESSAGE_LIST,
    PRIMITIVE_MAP,
    MAP_TO_MESSAGE,
    OTHER
}
