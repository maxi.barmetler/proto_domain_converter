package org.silbertb.proto.domainconverter.annotations;


import org.silbertb.proto.domainconverter.custom.NullConverter;
import org.silbertb.proto.domainconverter.custom.TypeConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Maps a field in a protobuf oneof group to corresponding field/class in the domain objects.
 * This annotation can be place above a class or a class member.
 * The mapping by default is done using the {@link #protoField()} name in the {@link #domainClass()}.
 * There are several ways to change the default behavior when this annotation is placed above a field:
 * 1. Standard mapping annotations
 * 2. Using {@link #domainField()} and optionally {@link #converter()} properties. This is handy, for example, in cases where the same domain class is used for several fields.
 * 3. Use @{@link ProtoConverter} on top of the domain class to convert the proto field to the whole domain class
 * 4. Use {@link #converter()} property without {@link #domainField()} to convert the proto field to the whole domain class
 * Used with conjunction of {@link OneofBase}
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface OneofField {
    /**
     *
     * @return the name of the protobuf field
     */
    String protoField();

    /**
     * @return The domain class of this field
     */
    Class<?> domainClass();

    /**
     * @return The name of the domain field (Optional)
     */
    String domainField() default "";

    /**
     * @return converter if needed. (Optional)
     */
    Class<? extends TypeConverter> converter() default NullConverter.class;
}
