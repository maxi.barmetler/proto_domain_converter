package org.silbertb.proto.domainconverter.annotations;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.custom.NullMapper;
import com.google.protobuf.Message;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Map between a domain class to a protobuf generated class.
 * The annotated class is the domain class
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoClass {
    /**
     *
     * @return The class generated from the protobuf message
     */
    Class<? extends Message> protoClass();

    /**
     * By default, use other annotations for mapping, for example {@link ProtoField}
     * If return a mapper then the mapper is responsible for the conversion between the domain class and the protobuf generated class
     * @deprecated use {@link ProtoClassMapper}
     * @return mapper better domain class and protobuf generated class
     */
    @Deprecated
    Class<? extends Mapper> mapper() default NullMapper.class;

    /**
     * Consider annotated fields in super classes as well.
     * The default is false.
     * @return If true then annotated field in super classes will be considered.
     */
    boolean withInheritedFields() default false;

    /**
     * "whitelist" mode means that each field that we want to map from protobuf message need to marked with {@link ProtoField} annotation.
     * "blacklist" mode means that each field is mapped by default as if it was annotated with empty {@link ProtoField}. The default can be changed using of the other field annotations and specifically by using {@link ProtoIgnore} which means that the annotated field is not mapped at all.
     *
     * @return false for "whitelist" mode and true for "blacklist" mode
     */
    boolean blacklist() default false;
}
